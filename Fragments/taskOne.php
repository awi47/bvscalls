<?php
require '../dashboardConnect.php';

try {
    //Number of calls for current year to date
    $total_number_calls_query = 'SELECT COUNT(*) FROM BVSCalls WHERE CallTime <= CURDATE()';
    //Total Cost for current year to date
    $total_cost_query = 'SELECT sum(Cost) FROM BVSCalls WHERE CallTime BETWEEN date(YEAR(CURDATE())) AND date(CURDATE())';
    $total_number_calls_stmt = $dbConn->prepare($total_number_calls_query);
    $total_cost_stmt = $dbConn->prepare($total_cost_query);
    $result = $total_number_calls_stmt->execute();
    $result = $total_cost_stmt->execute();

    echo "Number of Calls To Date:" . $total_number_calls_stmt->fetchColumn(); 
    echo "<br>Total Cost: R" . $total_cost_stmt->fetchColumn();
}
catch (PDOException $e) {
    echo 'PDO Exception Caught.  ';
    echo 'Error with the database: <br />';
    echo 'SQL Query: ', $total_number_calls_query;
    echo 'SQL Query: ', $total_cost_query;
    echo 'Error: ' . $e->getMessage();
}
?>
