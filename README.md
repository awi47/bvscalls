
# RT ASSESSMENT

This is a single table used to capture details from a telephone system. 
The client would like to view the calls by Year/Month/Extension

## Develop the following pages:

1. Dashboard page showing a bar graph with two columns
- Number of calls
- Total Cost
- By Month for the current year Jan - to date

2. Table below this graph with the following columns 
- Year-Month (YYYY-MMM)
- Number of calls
- Total Cost

3. On the Year-Month Column I want a hyperlink to page 2 - A summary by month
- Extension
- Number of calls for the month
- Total value for the month

4. On the Extension column a hyperlink to page 3 - Detail list of all calls by extension
- This will show the detail of the selected month
- Title on the page to show Year-Month / Extension
- Table Columns
    - Extension - Call From
    - Destination - Call To
    - Time of call
    - Duration
    - Cost