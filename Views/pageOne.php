<?php
/**
1. Dashboard page showing a bar graph with two columns
- Number of calls
- Total Cost
- By Month for the current year Jan - to date
2. Table below this graph with the following columns 
- Year-Month (YYYY-MMM)
- Number of calls
- Total Cost
*/
require '../Fragments/head.php';
require '../Fragments/taskOne.php';
require '../Fragments/taskTwo.php';
require '../Fragments/footer.php';
?>
