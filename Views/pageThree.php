<?php
/**
4. On the Extension column a hyperlink to page 3 - Detail list of all calls by extension
 - This will show the detail of the selected month
 - Title on the page to show Year-Month / Extension
 - Table Columns
     - Extension - Call From
     - Destination - Call To
     - Time of call
     - Duration
     - Cost
*/
require '../Fragments/head.php';
require '../Fragments/taskFour.php';
require '../Fragments/footer.php';
?>