<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'BVSCalls';
 
// Table's primary key
$primaryKey = 'CallFrom';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'CallFrom', 'dt' => 0 ),
    array( 'db' => 'CallTo',  'dt' => 1 ),
    //Year-Month (YYYY-MMM)
    array( 'db' => 'CallTime','dt' => 2,
        'formatter' => function( $d, $row ) {
            return date( 'Y-m', strtotime($d));
        }
    ),
    array( 'db' => 'Duration', 'dt' => 3, ),
    //Cost
    array( 'db' => 'BILLING', 'dt' => 4,),
    // array( 'dt' => 'Cost', 'dt' => 5,),
    // array( 'dt' => 'Status', 'dt' => 5,),
   
);
 
// SQL server connection information
$sql_details = array(
    "type" => "Mysql",
    "dsn" => 'charset=utf8mb4',
    'user' => 'root',
    'pass' => 'root',
    'db'   => 'rito_db',
    'host' => 'localhost'
);

?>