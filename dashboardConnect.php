<?php
$dbHost = 'localhost';
$dbName = 'rito_db';
$dbUsername = 'root';
$dbPassword = 'root';

try {
	$dbConn = new PDO("mysql:host={$dbHost};dbname={$dbName}", $dbUsername, $dbPassword);
	$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo $e->getMessage();
}

?>