    /**
    Tell DataTables to work its magic on the table
    wait for the document to be fully ready, and then select the table we want, and
     run the DataTables function on it.

   Server-side processing in DataTables is enabled through use of the serverSide option.
   Simply set it to true and DataTables will operate in server-side processing mode.
     You will also want to use the ajax option to specify the URL where DataTables should get its Ajax data from.
    */
$(document).ready( function () {
    $('#table').DataTable({
        paging: true,
        select: true,
        "processing": true,
        "serverSide": true,
        ajax: '../data.php',
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [3],
                // "render": function ( data, type, full, meta ) {
                //     return '<a href="'+aspx+'">Download</a>';}, 
                "defaultContent": "Click to edit"
            },
        ],
    "footerCallback": function () {
        var api = this.api();
        var results = api;
            $(results.column(2).footer()).html(
                "YYYY-MM"
            );
            $(results.column(3).footer()).html(
                "Number of calls " + api.column(3).data().count()
            );
            $(results.column(4).footer()).html(
                "Total Cost R" + api.column(4).data().sum()
            );
        },
    });

    $('#page_two_table').DataTable({
        paging: true,
        select: true,
        "processing": true,
        "serverSide": true,
        ajax: '../data.php',
    "footerCallback": function () {
        var api = this.api();
        var results = api;
            $(results.column("x").footer()).html(
                "YYYY-MM"
            );
            $(results.column("x").footer()).html(
                "Calls this month" //doCount
            );
            $(results.column("x").footer()).html(
                "Value this month" //doSum
            );
        },
    });
    
    $('#page_three_table').DataTable({
        paging: true,
        select: true,
        "processing": true,
        "serverSide": true,
        ajax: '../data.php',
    "footerCallback": function () {
        var api = this.api();
        var results = api;
            $(results.column("x").footer()).html(
                "Call From"
            );
            $(results.column("x").footer()).html(
                "Call To"
            );
            $(results.column("x").footer()).html(
                "Time of call " 
            );
            $(results.column("x").footer()).html(
                "Duration " 
            );
            $(results.column("x").footer()).html(
                "Cost" 
            );
        },
    });
});